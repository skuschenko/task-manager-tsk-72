package com.tsc.skuschenko.tm.api.service.dto;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(
            @Nullable User user, @Nullable String name,
            @Nullable String description
    );

    void addAll(@Nullable List<Task> tasks);

    @NotNull
    Task changeStatusById(@Nullable String id, @Nullable Status status);

    @NotNull
    Task changeStatusByIndex(@Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeStatusByName(@Nullable String name, @Nullable Status status);

    @SneakyThrows
    void clear();

    @NotNull
    Task completeById(@Nullable String id);

    @NotNull
    Task completeByIndex(@Nullable Integer index);

    @NotNull
    Task completeByName(@Nullable String name);

    @NotNull
    List<Task> findAll(@Nullable Comparator<Task> comparator);

    @Nullable
    List<Task> findAll();

    @Nullable
    Task findOneById(@Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable Integer index);

    @Nullable
    Task findOneByName(@Nullable String name);

    @Nullable
    Task removeOneById(@Nullable String id);

    @Nullable
    Task removeOneByIndex(@Nullable Integer index);

    @Nullable
    Task removeOneByName(@Nullable String name);

    @NotNull
    Task startById(@Nullable String id);

    @NotNull
    Task startByIndex(@Nullable Integer index);

    @NotNull
    Task startByName(@Nullable String name);

    @NotNull
    Task updateOneById(
            @Nullable String id, @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateOneByIndex(
            @Nullable Integer index, @Nullable String name,
            @Nullable String description
    );

}
