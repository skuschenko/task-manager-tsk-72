package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.exception.AbstractException;
import org.jetbrains.annotations.Nullable;

public final class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public IndexIncorrectException(@Nullable final String argument) {

        super("Error! Index '" +
                argument + "'. Index should only contain numbers...");
    }

    public IndexIncorrectException(@Nullable final Integer argument) {
        super("Error! Argument '" + argument + "' must be non-negative...");
    }

}
