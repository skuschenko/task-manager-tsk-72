package com.tsc.skuschenko.tm.service.model;

import com.tsc.skuschenko.tm.api.service.dto.ITaskService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.model.TaskRepository;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public final class TaskService extends AbstractBusinessService<Task>
        implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task add(
            @Nullable final User user, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        taskRepository.save(task);
        return task;
    }

    @Override
    public void addAll(@Nullable final List<Task> tasks) {
        Optional.ofNullable(tasks).ifPresent(
                items -> items.forEach(
                        item -> add(
                                item.getUser(),
                                item.getName(),
                                item.getDescription())
                )
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneById(id))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusByIndex(
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByIndex(index))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusByName(
            @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByName(name))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        taskRepository.save(task);
        return task;

    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        taskRepository.clearAllTasks();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task completeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneById(id))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task completeByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Task task =
                Optional.ofNullable(findOneByIndex(index))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        taskRepository.save(task);
        return task;

    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task completeByName(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByName(name))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final Comparator<Task> comparator) {
        Optional.ofNullable(comparator).orElse(null);
        return taskRepository.findAll()
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return taskRepository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return taskRepository.findOneByIndex().get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return taskRepository.findOneByName(name);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeOneById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Task task =
                Optional.ofNullable(taskRepository.findTaskById(id))
                        .orElseThrow(TaskNotFoundException::new);
        taskRepository.delete(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeOneByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Task task =
                Optional.ofNullable(findOneByIndex(index))
                        .orElseThrow(TaskNotFoundException::new);
        taskRepository.delete(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeOneByName(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByName(name))
                        .orElseThrow(TaskNotFoundException::new);
        taskRepository.delete(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task startById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Task task = Optional.ofNullable(findOneById(id))
                .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task startByIndex(@Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Task task = Optional.ofNullable(findOneByIndex(index))
                .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task startByName(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task = Optional.ofNullable(findOneByName(name))
                .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateOneById(
            @Nullable final String id, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task = Optional.ofNullable(findOneById(id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateOneByIndex(
            @Nullable final Integer index, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task = Optional.ofNullable(findOneByIndex(index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

}