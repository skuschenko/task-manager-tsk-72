package com.tsc.skuschenko.tm.api.endpoint;

import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @WebMethod
    @PostMapping("/create")
    void create(@RequestBody @NotNull final Project project);

    @WebMethod
    @PostMapping("/createAll")
    void createAll(@RequestBody @NotNull Collection<Project> projects);

    @WebMethod
    @DeleteMapping("/deleteAll")
    void deleteAll(@RequestBody @NotNull Collection<Project> projects);

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") @NotNull String id);

    @WebMethod
    @GetMapping("/findById/{id}")
    Project find(@PathVariable("id") @NotNull String id);

    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @WebMethod
    @PutMapping("/save")
    void save(@RequestBody @NotNull Project project);

    @WebMethod
    @PutMapping("/saveAll")
    void saveAll(@RequestBody @NotNull Collection<Project> projects);

}
