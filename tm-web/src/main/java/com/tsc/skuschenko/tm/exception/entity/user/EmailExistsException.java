package com.tsc.skuschenko.tm.exception.entity.user;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email already exist...");
    }

}
