package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.IUserDetailsServiceBean;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.model.CustomerUser;
import com.tsc.skuschenko.tm.model.Role;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("userDetailsService")
public class UserDetailsServiceBean implements IUserDetailsServiceBean {

    @Autowired
    @NotNull
    private IUserService userService;

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        return userService.findByLogin(login);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String username)
            throws UsernameNotFoundException {
        @Nullable final User user = findByLogin(username);
        Optional.ofNullable(user).orElseThrow(() ->
                new UsernameNotFoundException("User not found.")
        );
        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (Role role : userRoles) roles.add(role.toString());
        return new CustomerUser(
                org.springframework.security.core.userdetails.User
                        .withUsername(username)
                        .password(user.getPasswordHash())
                        .roles(roles.toArray(new String[]{}))
                        .build()
        ).withUserId(user.getId());
    }

}